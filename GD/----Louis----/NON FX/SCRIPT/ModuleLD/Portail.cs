﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portail : MonoBehaviour
{
    public static int cristauxInLevel;

    [Range (0f ,4f )]
    public int nombreDeCristaux;
    public GameObject[] cristauxSlot;
    public GameObject interieurPortail;
    public ParticleSystem[] FXs; 
    private Mesh mesh;
    public GameObject winScreen;
    bool isOuvert;

    private void Awake()
    {
        cristauxInLevel = nombreDeCristaux;
    }

    public void cristauxCheck()
    {
        UpdateFMOD();

        if (cristauxInLevel <= 0)
        {
            PortailOpen(); 
        }

    }

    public void UpdateFMOD()
    {
        MusicManager.musicManager.Crystal(cristauxSlot.Length - cristauxInLevel);
    }

    private void PortailOpen ()
    {
        interieurPortail.SetActive(true); 
        GetComponent<FMODUnity.StudioEventEmitter>().Play();
        isOuvert = true;
        if (FXs.Length == 0)
        {
            return;
        }
        else
        {
            foreach (ParticleSystem fx in FXs)
            {
                fx.Play(); 
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if((other.GetComponent<PlayerController>() != null || other.GetComponent<PlayerControllerDjuloh>() != null) && isOuvert)
        {
            isOuvert = false;
            MusicManager.musicManager.Win();
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 1);
            other.GetComponent<PlayerController>().cam.GetComponent<SecondCamera>().TransitionToNextLevel();
        }
    }
}

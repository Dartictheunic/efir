﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cristaux : MonoBehaviour , IActivatable
{
    private bool hasBeenActivated;
    Animator anim;
    public ParticleSystem[] FXs; 
    public ParticleSystem aura;
    public Portail portail;
    public GameObject myLine;

    private void Start()
    {
        VarsChecker.CheckIfVarisAssigned(portail, gameObject);
        anim = GetComponent<Animator>();
    }

    public void ActivateItem()
    {
        if (!hasBeenActivated)
        {
            hasBeenActivated = true;
            anim.SetTrigger("Activation Trigger");
            Portail.cristauxInLevel--;
            //rayon.transform.position = portail.cristauxSlot[Portail.cristauxInLevel].transform.position; 
            portail.cristauxCheck();
            GetComponent<FMODUnity.StudioEventEmitter>().SetParameter("Crystal 1", 1f);
            aura.Stop();
            CristauxActivation();
            if(myLine != null)
            myLine.SetActive(false);
            Debug.Log(Portail.cristauxInLevel);
        }
    }

    private void CristauxActivation ()
    {
        // anim.Play();
        if (FXs.Length == 0)
        {
            return;
        }
        else
        {
            foreach (ParticleSystem fx in FXs)
            {
                fx.Play();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>()!= null || other.GetComponent<PlayerControllerDjuloh>() != null)
        {
            Debug.Log("Cristal activé");
            ActivateItem();
        }
    }

}

   




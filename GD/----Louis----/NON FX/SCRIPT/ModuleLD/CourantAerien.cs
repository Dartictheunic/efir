﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CourantAerien : MonoBehaviour
{
    [Header ("Tweeking")]
    public Color cAscendant;
    public Color cDescandant;
    public float speedBoost;
    public AnimationCurve WindSoundCurve;
    [Range(0 , 5)]
    public float particlesSpeed;
    [Header("Tweeking editor")]
    public Color meshColorAscend; 
    public Color meshColorDescend;

    [Header("Tweak Rotations")]
    private float rotFactor = 3;
    private Quaternion rotBase, rotVoulue ;
    private bool canRotate;
    private GameObject playerCtrl;

    [Header ("type de courant aérien ")]
    public TypeDeCourant type = TypeDeCourant.ascendant;
    [Header ("Pas Touche")]
    public ParticleSystem fx;
    public LineRenderer lR;
    public GameObject entree, sortie;
    public int typeModif = 1;
    private Mesh mesh;
    private Vector3 direction, desiredPoint;
    public Color meshColor;
    
    Material lrMat;
    bool isPlayerInCourant;
    float timeInCourant;
    Vector4 baseTiling;
    Vector4 invertTiling;
    private void Start()
    {
        lrMat = lR.material;
        lR.material = lrMat;
        baseTiling = lrMat.GetVector("Vector2_19F1EDB6");
        invertTiling = baseTiling;
        invertTiling.x *= -1;
        direction = (sortie.transform.position - entree.transform.position).normalized;
        UpdateFxType();
    }
    private void Update()
    {
        if (isPlayerInCourant)
        {

         
            if (canRotate == true)
            {
                
                Rotate();
            }
        }
    }
    private void FixedUpdate()
    {
        if(isPlayerInCourant)
        {

            MusicManager.musicManager.InCourantDAir(WindSoundCurve.Evaluate(timeInCourant));
            timeInCourant += Time.deltaTime;
            
        }

        else if (timeInCourant > 0f)
        {
            MusicManager.musicManager.InCourantDAir(WindSoundCurve.Evaluate(timeInCourant));
            timeInCourant -= Time.deltaTime;
        }

        else if(timeInCourant < 0f)
        {
            MusicManager.musicManager.InCourantDAir(WindSoundCurve.Evaluate(0));
            timeInCourant = 0f;
        }
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null && !isPlayerInCourant)
        {
            isPlayerInCourant = true;
            other.GetComponent<PlayerController>().controller.StartTakeWind();
            other.GetComponent<PlayerController>().mustMoveForward = false;
            canRotate = true;
            if(typeModif == 1)
            {
                direction = (sortie.transform.position - other.transform.position).normalized;
            }

            else
            {
                direction = (entree.transform.position- other.transform.position).normalized;
            }

            ForcesDictionnaryScript.forcesDictionnaryScript.AddForce ("Courant" + transform.name , direction * speedBoost);
            
            rotBase = other.transform.rotation;

            playerCtrl = other.gameObject;

            Vibration.Vibrate(200);
            StartCoroutine(LaunchSecondTime(400f));
        }
    }
  
    public IEnumerator LaunchSecondTime(float time)
    {
        yield return new WaitForSecondsRealtime(time / 1000f);
        if(isPlayerInCourant)
        {
            Vibration.Cancel();
            Vibration.Vibrate(50);
            StartCoroutine(LaunchSecondTime(200f));
        }
    }
    public void Rotate()
    {
        

        //  Vector3 desiredPoint = Vector3.RotateTowards(playerCtrl.transform.forward, sortie.transform.position, rotFactor * Time.deltaTime, 0.0f);
        //  playerCtrl.transform.eulerAngles = Vector3.Lerp(playerCtrl.transform.position , desiredPoint, 1f);
    
       
        if((playerCtrl.transform.rotation.x / rotVoulue.x) <= 0.9f || (playerCtrl.transform.rotation.x / rotVoulue.x) >= 1.1f)
        {
            if(typeModif == 1)
            {
                desiredPoint = (sortie.transform.position - playerCtrl.transform.position).normalized;
            }
            if (typeModif == -1)
            {
                desiredPoint = (entree.transform.position - playerCtrl.transform.position).normalized;
            }
         
            rotVoulue = Quaternion.LookRotation(desiredPoint);

            rotFactor = Mathf.Lerp(rotFactor, 1f, Time.deltaTime * 4f);

            playerCtrl.transform.rotation = Quaternion.Slerp(playerCtrl.transform.rotation, rotVoulue, Time.deltaTime * rotFactor);
            
        }
        else
        {
            if((playerCtrl.transform.rotation.z) == 0)
            {
                canRotate = false;
            }
            Debug.Log("c'est bon");

         //   playerCtrl.transform.rotation = Quaternion.Slerp(playerCtrl.transform.rotation, new Quaternion(playerCtrl.transform.rotation.x, playerCtrl.transform.rotation.y, 0, 0), Time.deltaTime * 10);
            
            

        }
        //POUBELLE
        //  var desiredRotQ = Quaternion.Euler(direction.x, direction.y, direction.z);
        // playerCtrl.transform.LookAt(sortie.transform.position);
        //  Quaternion desiredRotQ = Quaternion.LookRotation(desiredPoint, Vector3.up);
        //  Debug.Log(playerCtrl.transform.rotation);
        //   playerCtrl.transform.eulerAngles = Vector3.Lerp(playerCtrl.transform.position, desiredPoint, 2f) ;
        //  playerCtrl.transform.rotation = desiredRotQ;
        //  playerCtrl.transform.rotation = Quaternion.Lerp(playerCtrl.transform.rotation, new Quaternion(transform.rotation.x + 50, transform.rotation.y + 50 , 0, 0), rotFactor * Time.deltaTime);
        //  playerCtrl.transform.rotation = new Quaternion(playerCtrl.transform.rotation.x, playerCtrl.transform.rotation.y, playerCtrl.transform.rotation.z,0);
        //  playerCtrl.gameObject.transform.rotation = new Quaternion(Mathf.LerpAngle(rotBase.x, direction.x, rotXfactor * Time.deltaTime), Mathf.LerpAngle(rotBase.y, direction.y, rotYfactor * Time.deltaTime), Mathf.LerpAngle(rotBase.z, direction.z, rotZfactor * Time.deltaTime),0);


    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null && isPlayerInCourant)
        {
            ForcesDictionnaryScript.forcesDictionnaryScript.RemoveForce("Courant" + transform.name);
            isPlayerInCourant = false;
            other.GetComponent<PlayerController>().mustMoveForward = true;
            other.GetComponent<PlayerController>().controller.StopTakeWind();
            other.transform.eulerAngles = new Vector3(other.transform.eulerAngles.x, other.transform.eulerAngles.y, 0f);
            Vibration.Cancel();
        }
    }

    public void ChangeCourantType()
    {
        switch (type)
        {
            case TypeDeCourant.ascendant:
                {
                    type = TypeDeCourant.descendant;
                    UpdateFxType();
                }
                break;

            case TypeDeCourant.descendant:
                {
                    type = TypeDeCourant.ascendant;
                    UpdateFxType();
                }
                break;

        }
    }

    public void UpdateFxType()
    {
        switch (type)
        {
            case TypeDeCourant.ascendant:
                {
                    typeModif = 1;
                    print(typeModif);
                    fx.transform.position = entree.transform.position;
                    ParticleSystem.VelocityOverLifetimeModule psVel = fx.velocityOverLifetime;
                    psVel.yMultiplier = particlesSpeed * typeModif;
                    ParticleSystem.MainModule psMain = fx.main;
                    lrMat.SetColor("Color_695193B9", cAscendant);
                    lrMat.SetVector("Vector2_19F1EDB6", baseTiling);
                    psMain.startColor = cAscendant;
                    print("ascent");
                }
                break;

            case TypeDeCourant.descendant:
                {
                    typeModif = -1;
                    print(typeModif);
                    fx.transform.position = sortie.transform.position;
                    ParticleSystem.VelocityOverLifetimeModule psVel = fx.velocityOverLifetime;
                    psVel.yMultiplier = particlesSpeed * typeModif;
                    ParticleSystem.MainModule psMain = fx.main;
                    lrMat.SetColor("Color_695193B9", cDescandant);
                    lrMat.SetVector("Vector2_19F1EDB6", invertTiling);
                    psMain.startColor = cDescandant;
                    print("descant");
                }
                break;

        }

    }

    //juste pour qu'on voit bien 
    void OnDrawGizmos()
    {
        mesh = GetComponent<MeshFilter>().sharedMesh;
        Gizmos.color = meshColor;
        Gizmos.DrawMesh(mesh, transform.position, transform.localRotation, transform.localScale);
    }

}

public enum TypeDeCourant
{
    ascendant,
    descendant
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour, IActivatable
{
    public CourantAerien[] mesCourants;
    public Color upColor;
    public Color downColor;
    public LineRenderer[] myLr;
    public ParticleSystem[] fx;
    Material myMat;
    bool red;
    // Start is called before the first frame update
    void Start()
    {
        myMat = GetComponent<MeshRenderer>().material;
    
        VarsChecker.CheckIfVarisAssigned(myLr, gameObject);

        foreach (CourantAerien courant in mesCourants)
        {
            if (courant.type == TypeDeCourant.ascendant)
            {
                foreach (LineRenderer link in myLr)
                {
                    link.material = link.material;
                    link.material.SetColor("Color_48E9C892", upColor);

                }
                myMat.color = upColor;
            }

            else
            {
                foreach (LineRenderer link in myLr)
                {
                    link.material = link.material;
                    link.material.SetColor("Color_48E9C892", downColor);
                }
                myMat.color = downColor;
            }
        }
    }

    public void ActivateItem()
    {
        ChangeMyCourant();
        foreach(ParticleSystem aled in fx)
        {
            aled.Play();
        }
        GetComponent<FMODUnity.StudioEventEmitter>().Play();
    }

    public void ChangeMyCourant()
    {

        foreach (CourantAerien courant in mesCourants)
        {
            courant.ChangeCourantType();
            if (courant.type == TypeDeCourant.ascendant)
            {
                foreach (LineRenderer link in myLr)
                {
                    link.material = link.material;
                    link.material.SetColor("Color_48E9C892", upColor);

                }
                myMat.color = upColor;
            }

            else
            {
                foreach (LineRenderer link in myLr)
                {
                    link.material = link.material;
                    link.material.SetColor("Color_48E9C892", downColor);
                }
                myMat.color = downColor;
            }
        }

     
    }
}

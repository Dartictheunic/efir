﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CercleAura : MonoBehaviour
{
    public AnimationCurve boostCurve;
    private Vector3 target; 
    public float baseForwardSpeed;
    public float curveTime = 4f;
    public float duration ;
    public long[] pattern;

    // Update is called once per frame 
    void FixedUpdate()
    {
        if (curveTime < duration)
        {
            applyBoostAura();
        }
    }

    public void applyBoostAura()
    {
        curveTime += Time.deltaTime;
        float curveAmount = boostCurve.Evaluate(curveTime) * baseForwardSpeed;
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Boost " + transform.name, target * curveAmount); 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerControllerDjuloh>() != null || other.GetComponent<PlayerController>() != null)
        {     
            startBoostAura();
            target = other.transform.forward;
            StartCoroutine(StopAcceleration());
        }
    }

    public void startBoostAura()
    {
        curveTime = 0f;
        Vibration.Vibrate(100);
    }

    private IEnumerator StopAcceleration()
    {
        yield return new WaitForSeconds(duration + 0.1f);
        ForcesDictionnaryScript.forcesDictionnaryScript.RemoveForce("Boost " + transform.name);
    }
}
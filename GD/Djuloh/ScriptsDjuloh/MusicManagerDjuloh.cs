﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManagerDjuloh : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string music = "event:/BGM";

    FMOD.Studio.EventInstance musicEvent;


    public static MusicManagerDjuloh musicManagerDjuloh;
    public PlayerControllerDjuloh player;
    public GameObject mainMenu;

    // Start is called before the first frame update
    void Start()
    {

        musicEvent = FMODUnity.RuntimeManager.CreateInstance(music);
        musicEvent.start();
        musicManagerDjuloh = this;
    }

    //Player starts the game
    public void GameStarted()
    {
        musicEvent.setParameterValue("Game Started", 1f);
        player.ResetPlayer();
        player.mustMoveForward = true;
        mainMenu.SetActive(false);
    }


    //Player récupère 1er crystal
    public void Crystal(int number)
    {
        if (number == 1)
        {
            musicEvent.setParameterValue("Crystal " + number.ToString(), 1f);
        }

        else
        {
            musicEvent.setParameterValue("Crystal " + (number - 1).ToString(), 0f);
            musicEvent.setParameterValue("Crystal " + number.ToString(), 1f);
        }
    }
    public void FirstCrystal()
    {
        musicEvent.setParameterValue("Crystal 1", 1f);
    }

    //Player récupère 2eme crystal
    public void SecondCrystal()
    {
        musicEvent.setParameterValue("Crystal 1", 0f);
        musicEvent.setParameterValue("Crystal 2", 1f);
    }

    //Player récupère 3eme crystal
    public void ThirdCrystal()
    {
        musicEvent.setParameterValue("Crystal 2", 0f);
        musicEvent.setParameterValue("Crystal 3", 1f);
    }

    public void InCourantDAir(float value)
    {
        musicEvent.setParameterValue("isInCourantdAir", value);
    }

    public void Win()
    {
        musicEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        musicEvent.release();
        SceneManager.LoadScene(0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraDjuloh : MonoBehaviour
{
    public Transform target;
    public float valueLimit;
    Vector3 lookOffset;

    private void LateUpdate()
    {
        transform.LookAt(target.position + lookOffset);
    }

    public void ChangeOffset(float xOffset, float yOffset, float zOffset)
    {
        lookOffset = new Vector3(xOffset, yOffset, zOffset);
    }
}

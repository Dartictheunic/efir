﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homebrew;

public class PlayerFollowerDjuloh : MonoBehaviour
{
    public Transform behindPlayer;
    public Transform player;
    [Tooltip("Vitesse à laquelle la caméra s'avance vers le joueur en cas de hit")]
    public float blockedSpeed;


    private void LateUpdate()
    {
        Vector3 direction = behindPlayer.position - transform.position;
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, direction.magnitude, ~0, QueryTriggerInteraction.Ignore))
        {
            //mybody.MovePosition(behindPlayer.position);
            transform.position = Vector3.Lerp(transform.position, behindPlayer.position, Time.deltaTime);

        }
        else
        {
            Vector3 directionSafe = behindPlayer.position - player.position;
            Ray raySafe = new Ray(player.position, directionSafe);
            Debug.DrawRay(player.position, directionSafe, Color.red, 5f, false);
            RaycastHit hitSafe;
            if (!Physics.Raycast(raySafe, out hitSafe, directionSafe.magnitude, ~0, QueryTriggerInteraction.Ignore))
            {
                transform.position = Vector3.Lerp(transform.position, behindPlayer.position, Time.deltaTime);
            }

            else if (!hit.collider.isTrigger)
            {
                transform.position = Vector3.Lerp(transform.position, hitSafe.point, Time.deltaTime);

                Debug.DrawRay(transform.position, (hitSafe.transform.position - directionSafe / 10), Color.green, 5f);
                //mybody.MovePosition(hitSafe.transform.position - directionSafe.normalized);
            }
        }
    }
}

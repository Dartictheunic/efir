﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using Homebrew;


public class PlayerControllerDjuloh : MonoBehaviour
{
    #region Variables Feel
    [Foldout("Variables déplacement", true)]
    [Tooltip("Vitesse à laquelle le joueur avance devant lui")]
    [Range(.0001f, 1f)]
    public float forwardSpeed;
    [Tooltip("Influence de la rotation du joueur sur sa vitesse")]
    public float rotationInfluence;
    [Tooltip("Puissance de la gravité sur le joueur")]
    public float gravity = -0.0001f;
    [Foldout("Variables rotation", true)]
    public float rotationSpeed = .1f;
    public float maxXRotation = 40f;
    public float maxYRotation = 5f;
    [Tooltip("Angle que l'on peut donner au téléphone au maximum")]
    public float maxInputTaken = .25f;
    [Tooltip("Multiplicateur secondaire de l'angle du téléphone sur la rotation")]
    public AnimationCurve rotationCurve;
    [Foldout("Variables fly", true)]
    [Tooltip("Temps où le joueur peut incliner le téléphone vers le haut de base")]
    public float essenceDeSecours = 100f;
    [Tooltip("Fenetre pendant laquelle le brackage est checké")]
    public float timeForBraquage = .05f;
    [Tooltip("Multiplicateur d'énergie du piqué")]
    [Range(0f, 1f)]
    public float piqueMultiplicator = .8f;


    [Foldout("Variables éditeur", true)]
    [Tooltip("A quel point la souris transmet d'input")]
    public float mouseScale = 80f;
    #endregion


    #region liens à faire
    [Foldout("Variables à link", true)]
    [Space(30)]
    [Tooltip("La caméra dans la scène")]
    public Camera cam;
    [Tooltip("Le trigger en enfant du player (souvent une sphère)")]
    public PlayerActivator trigger;
    [Tooltip("Ce qui gère mesh & anim")]
    public PlayerMeshController controller;
    #endregion

    [Foldout("Autres (prog, debug etc...)", true)]
    #region variables Prog
    [Header("Variables pour la prog, mainly")]
    [Tooltip("L'état actuel du joueur, public pour simplifier le débug")]
    public PlayerState actualPlayerState = PlayerState.flying;
    [Tooltip("Le joueur doit-il avancer ?")]
    public bool mustMoveForward = false;
    [Tooltip("Energie accumulée en piqué")]
    public float energieAccumuleePique;
    [Tooltip("Le vecteur 3 appliqué au joueur à la frame précédente")]
    public Vector3 playerLastMovement;

    #region debug text
    [Header("Debug ou temporaire")]
    public long[] vibrationsDucul;
    public Text textDebug1;
    public Text textDebug2;
    public Text textDebug3;
    public Text textDebug4;
    public Text textDebug5;
    public Text textDebug6;
    #endregion

    Vector3 basePhoneAngle;
    Vector3 truePhoneDelta;
    Vector3 targetRotation;
    Vector3 myEulerAngles;
    float xAccelerationDelta;
    float lastXRotation;
    float lastYPosition;
    bool invertHorizontal;
    bool invertVertical;
    float yDelta;
    float timeBeforeResetRotation;
    float zAccelerationDelta;
    float timeSpentInPique;
    float actualEssence;
    Rigidbody playerBody;

    #endregion

    public IEnumerator CheckSmartphoneAngle()
    {
        yield return new WaitForSeconds(.1f);
        basePhoneAngle = Input.acceleration;
    }

    public void ResetPhoneAngles()
    {
        basePhoneAngle = Input.acceleration;
    }

    public void SetRotationSpeed(float newSpeed)
    {
        rotationSpeed = newSpeed;
    }

    private void Start()
    {
        Input.gyro.enabled = true;
        playerBody = GetComponent<Rigidbody>();
        StartCoroutine(CheckSmartphoneAngle());
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Gravity", new Vector3(0, gravity, 0));
        actualEssence = essenceDeSecours;
    }

    public void ChangePlayerGravity(float newGravityMultiplier)
    {
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Gravity", new Vector3(0, newGravityMultiplier, 0));
    }

    public void ResetPlayerGravity()
    {
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Gravity", new Vector3(0, gravity, 0));
    }

    public void ResetPlayer()
    {
        basePhoneAngle = Input.acceleration;
        transform.rotation = Quaternion.identity;
        targetRotation = Vector3.zero;
        transform.position = Vector3.zero;
    }

    private void FixedUpdate()
    {
        yDelta = lastYPosition - transform.position.y;
        Move();

#if !UNITY_EDITOR
        UpdatePlayer();
#endif

#if UNITY_EDITOR
        EditorControls();
#endif


        lastYPosition = transform.position.y;
    }



    #region Gestion Deplacement
    public IEnumerator TemporaryReturnOfDrop(float DropTime)
    {
        yield return new WaitForSeconds(DropTime);
        mustMoveForward = true;
        ForcesDictionnaryScript.forcesDictionnaryScript.RemoveForce("Drop");
        actualPlayerState = PlayerState.pique;
        controller.StartPique();
    }

    public IEnumerator TemporaryEndAscend(float AscendTime)
    {
        yield return new WaitForSeconds(AscendTime);
        mustMoveForward = true;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
        actualPlayerState = PlayerState.flying;
    }

    public void Drop()
    {
        actualPlayerState = PlayerState.drop;
        mustMoveForward = false;
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Drop", transform.forward * forwardSpeed / 100);
        transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, maxXRotation, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);
        timeSpentInPique += Time.deltaTime;
        // AJOUTER UNE LIGNE POUR RAMENER LA CAMERA EN POSITION AU DESSUS DU JOUEUR
        StartCoroutine(TemporaryReturnOfDrop(1.5f));
    }

    public void UpdatePlayer()
    {
        Vector3 phoneRotations = GetPhoneRotations();
        textDebug1.text = phoneRotations.ToString();

        if (actualPlayerState == PlayerState.flying)
        {
            if (phoneRotations.x < -10f)
            {
                if (energieAccumuleePique > 0f)
                {
                    energieAccumuleePique += yDelta;
                }

                else if (actualEssence > 0f)
                {
                    actualEssence += yDelta;
                    Mathf.Clamp(actualEssence, -.2f, essenceDeSecours);
                }

                else
                {
                    //Drop();
                    return;
                }
            }

            else
            {
                if (actualEssence < essenceDeSecours)
                {
                    Mathf.Clamp(actualEssence, -.2f, essenceDeSecours);
                }
            }

#pragma warning disable CS0618 // Type or member is obsolete
            transform.RotateAround(Vector3.up, phoneRotations.y * Time.deltaTime * rotationSpeed);
#pragma warning restore CS0618 // Type or member is obsolete


            transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, phoneRotations.x, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);

            /*if (phoneRotations.x > 35)
            {
                actualPlayerState = PlayerState.pique;
                controller.StartPique();
            }*/

        }

        else if (actualPlayerState == PlayerState.pique)
        {
            if (phoneRotations.x < 35f)
            {

                if (timeSpentInPique > 1f)
                {
                    actualPlayerState = PlayerState.flying;
                    controller.StopPique();
                }

                else
                {
                    energieAccumuleePique += yDelta * piqueMultiplicator;
                }
            }

            else
            {
                energieAccumuleePique += yDelta * piqueMultiplicator;
            }

#pragma warning disable CS0618 // Type or member is obsolete
            transform.RotateAround(Vector3.up, phoneRotations.y * Time.deltaTime * rotationSpeed);
#pragma warning restore CS0618 // Type or member is obsolete

            transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, maxXRotation, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);
            timeSpentInPique += Time.deltaTime;

        }

        else if (actualPlayerState == PlayerState.forceAscend)
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(Mathf.Lerp(myEulerAngles.x, -maxXRotation, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z), Time.deltaTime);
            // METTRE DU CODE POUR METTRE LA CAMERA SOUS LE JOUEUR                 

            return;
        }

        else if (actualPlayerState == PlayerState.drop)
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(Mathf.Lerp(myEulerAngles.x, maxXRotation, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z), Time.deltaTime);
            // METTRE DU CODE POUR METTRE LA CAMERA AU DESSUS LE JOUEUR                 

        }

    }

    public void Move()
    {
        if (mustMoveForward)
        {
            playerLastMovement = (forwardSpeed - transform.forward.y * rotationInfluence) * transform.forward + ForcesDictionnaryScript.forcesDictionnaryScript.ReturnAllForces();
            playerBody.MovePosition(transform.position + playerLastMovement);
        }

        else
        {
            playerLastMovement = ForcesDictionnaryScript.forcesDictionnaryScript.ReturnAllForces();
            playerBody.MovePosition(transform.position + ForcesDictionnaryScript.forcesDictionnaryScript.ReturnAllForces());
        }
    }


    public Vector3 GetPhoneRotations()
    {
        truePhoneDelta = Input.acceleration - basePhoneAngle;
        textDebug3.text = truePhoneDelta.ToString();
        xAccelerationDelta = -truePhoneDelta.x;
        zAccelerationDelta = (truePhoneDelta.y - truePhoneDelta.z) / 2;

        if (timeBeforeResetRotation > timeForBraquage)
        {
            if (Mathf.Abs(lastXRotation - zAccelerationDelta) > .35f)
            {
                if (actualPlayerState == PlayerState.flying)
                {
                    Drop();
                }

                else if (actualPlayerState == PlayerState.pique && zAccelerationDelta < .1f)
                {
                    ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("FinPique", new Vector3(0f, energieAccumuleePique / timeSpentInPique, 0f), timeSpentInPique, false);
                    actualPlayerState = PlayerState.forceAscend;
                    StartCoroutine(TemporaryEndAscend(timeSpentInPique));
                }
            }

            else
            {
                //pas de braquage
            }

            timeBeforeResetRotation = 0f;
            lastXRotation = zAccelerationDelta;
        }

        else
        {
            timeBeforeResetRotation += Time.deltaTime;
        }

        float newXRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, targetRotation.x);
        float newYRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, targetRotation.z);

        newXRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, zAccelerationDelta);

        newYRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, xAccelerationDelta);


        Vector3 calculatedVector = new Vector3(Mathf.Lerp(-maxXRotation, maxXRotation, newXRotation), Mathf.Lerp(-maxYRotation, maxYRotation, newYRotation), 0);

        if (!invertHorizontal)
        {
            calculatedVector.y *= -1;
        }

        if (invertVertical)
        {
            calculatedVector.x *= -1;
        }
        return calculatedVector;
    }

    #endregion

    private void Update()
    {
#if !UNITY_EDITOR
        if (Input.touchCount > 0 && !trigger.activated && actualPlayerState == PlayerState.flying)
        {
            long mabite = 10;
            Vibration.Vibrate(mabite);

            trigger.Activate();
            controller.SetInteractingBool(true);
            actualPlayerState = PlayerState.interacting;
        }

        else if (Input.touchCount == 0 && trigger.activated)
        {
            trigger.DeActivate();
            controller.SetInteractingBool(false);
            actualPlayerState = PlayerState.flying;
        }
#endif
        myEulerAngles = returnGoodEulers(transform.eulerAngles);

#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && !trigger.activated && actualPlayerState == PlayerState.flying)
        {
            trigger.Activate();
            mustMoveForward = false;
            controller.SetInteractingBool(true);
            actualPlayerState = PlayerState.interacting;
        }

        else if (Input.GetMouseButtonUp(0) && trigger.activated)
        {
            trigger.DeActivate();
            mustMoveForward = true;
            controller.SetInteractingBool(false);
            actualPlayerState = PlayerState.flying;
        }

#endif
    }

    public Vector3 returnGoodEulers(Vector3 vectorToReturn)
    {
        if (vectorToReturn.x > 180)
        {
            vectorToReturn.x -= 360;
        }
        if (vectorToReturn.y > 180)
        {
            vectorToReturn.y -= 360;
        }
        if (vectorToReturn.z > 180)
        {
            vectorToReturn.z -= 360;
        }

        return vectorToReturn;

    }

    #region Debug

    public void EditorControls()
    {
        Vector3 mouseRotation = cam.ScreenToViewportPoint(Input.mousePosition);
        mouseRotation -= new Vector3(.5f, .5f);
        mouseRotation *= mouseScale;
        targetRotation = new Vector3(-mouseRotation.y, mouseRotation.x);


#pragma warning disable CS0618 // Type or member is obsolete
        transform.RotateAround(Vector3.up, targetRotation.y * Time.deltaTime * Time.deltaTime);
#pragma warning restore CS0618 // Type or member is obsolete

        //transform.Rotate(0, 1, 0, Space.World);

        transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, targetRotation.x, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);
    }

    public void InvertHorizontal(bool doyou)
    {
        invertHorizontal = doyou;
    }

    public void InvertVertical(bool doyou)
    {
        invertVertical = doyou;
    }
    #endregion

    public enum PlayerState
    {
        grounded,
        flying,
        interacting,
        drop,
        pique,
        forceAscend
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEdges : MonoBehaviour
{
    public Transform mapCenter;
    public Transform player;
    public AnimationCurve distanceAC;
    public AnimationCurve checkedDistanceAC;
    SphereCollider borderCollider;

    private void Start()
    {
        borderCollider = GetComponent<SphereCollider>();
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerController>() != null)
        {
            other.transform.LookAt(mapCenter);
        }
    }

    public void CheckDistanceToPlayer()
    {
        if(Vector3.Distance(player.position, transform.position) > borderCollider.radius * 0.9f && borderCollider.enabled == false)
        {
            borderCollider.enabled = true;
        }

        else if (borderCollider.enabled == true)
        {
            borderCollider.enabled = false;
        }
        
    }

    private void FixedUpdate()
    {
        if (Time.frameCount % 10 == 0)
        {
            CheckDistanceToPlayer();
        }
    }
}

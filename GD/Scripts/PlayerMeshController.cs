﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMeshController : MonoBehaviour
{
    public Animator playerAnimator;
    public SkinnedMeshRenderer playerMesh;

    public void StartDrop()
    {
        playerAnimator.SetTrigger("Drop");
    }

    public void EndDrop()
    {

    }

    

    public void StartPique()
    {
        playerAnimator.SetBool("Pique", true);
    }

    public void StopPique()
    {
        playerAnimator.SetBool("Pique", false);
    }

    public void StartTakeWind()
    {
        playerAnimator.SetBool("TakeWind", true);
    }

    public void StopTakeWind()
    {
        playerAnimator.SetBool("TakeWind", false);
    }


    public void StartDiveCinematique()
    {
        playerAnimator.SetBool("CineDive",true);
    }

    public void StopDiveCinematique()
    {
        playerAnimator.SetBool("CineDive", false);
    }

    public void SetInteractingBool(bool isInteracting)
    {
        if(playerAnimator.GetBool("Interacting") != isInteracting)
        {
            playerAnimator.SetBool("Interacting", isInteracting);
        }
    }

    public void SetOffset(float newOffset)
    {
        playerAnimator.SetFloat("Offset", (newOffset));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager menuManager;

    public Animator cineCamAnimator;
    public Animator mainCamAnimator;
    public GameObject startMenu;
    public GameObject skipButton;
    public string levelToLoad;
    bool did;
    // Start is called before the first frame update
    void Start()
    {
        menuManager = this;
    }

    public void FirstTapOnScreen()
    {
        cineCamAnimator.SetBool("Begin", true);
        startMenu.SetActive(false);
        skipButton.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null && !did)
        {
            did = true;
            mainCamAnimator.SetBool("Devant cristal", true);
            TimeManager.timeManager.HitFreeze(2f);
        }
    }

    public void LoadGoodScene()
    {
        MusicManager.musicManager.ReleaseMusic();
        SceneManager.LoadScene("Level " + levelToLoad);
    }
}
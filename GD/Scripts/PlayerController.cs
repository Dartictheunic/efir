﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;
using Homebrew;

public class PlayerController : MonoBehaviour
{
    #region Variables Feel
    [Foldout("Variables déplacement", true)]
    [Tooltip("Vitesse à laquelle le joueur avance devant lui")]
    [Range(.0001f, 1f)]
    public float forwardSpeed;
    [Tooltip("Influence de la rotation du joueur sur sa vitesse")]
    public float rotationInfluence;
    [Tooltip("Puissance de la gravité sur le joueur")]
    public float gravity = -0.0001f;
    [Foldout("Variables rotation", true)]
    public float rotationSpeed = .1f;
    public float maxXRotation = 40f;
    public float maxYRotation = 5f;
    [Tooltip("Angle que l'on peut donner au téléphone au maximum")]
    public float maxInputTaken = .25f;
    [Tooltip("Multiplicateur secondaire de l'angle du téléphone sur la rotation")]
    public AnimationCurve rotationCurve;
    [Foldout("Variables fly", true)]
    [Tooltip("Temps où le joueur peut incliner le téléphone vers le haut de base")]
    public float essenceDeSecours = 100f;
    [Tooltip("Fenetre pendant laquelle le brackage est checké")]
    public float timeForBraquage = .05f;
    [Tooltip("Multiplicateur d'énergie du piqué")]
    [Range(0f, 1f)]
    public float piqueMultiplicator = .8f;

    [Foldout("Variables Activation", true)]
    public AnimationCurve theWarudoTimeCurve;
    public AnimationCurve theWarudoSpeedCurve;

    [Foldout("Variables éditeur", true)]
    [Tooltip("A quel point la souris transmet d'input")]
    public float mouseScale = 80f;
    #endregion


    #region liens à faire
    [Foldout("Variables à link", true)]
    [Space(30)]
    [Tooltip("La caméra dans la scène")]
    public Camera cam;
    [Tooltip("Le trigger en enfant du player (souvent une sphère)")]
    public PlayerActivator trigger;
    [Tooltip("Ce qui gère mesh & anim")]
    public PlayerMeshController controller;
    [Tooltip("Le transform qui suit le player")]
    public PlayerFollower follower;
    [Tooltip("Le plane en haut de la map")]
    public OuterPlanes topPlane;
    [Tooltip("Le toggle pour l'horizontal")]
    public Toggle horizontalToggle;
    [Tooltip("Le toggle pour le vertical")]
    public Toggle verticalToggle;
    [Tooltip("Le slider pour la rotation")]
    public Slider rotationSlider;
    #endregion

    [Foldout("Autres (prog, debug etc...)", true)]
    #region variables Prog
    [Header("Variables pour la prog, mainly")]
    [Tooltip("L'état actuel du joueur, public pour simplifier le débug")]
    public PlayerState actualPlayerState = PlayerState.flying;
    [Tooltip("Le joueur doit-il avancer ?")]
    public bool mustMoveForward = false;
    [Tooltip("Energie accumulée en piqué")]
    public float energieAccumuleePique;
    [Tooltip("Le vecteur 3 appliqué au joueur à la frame précédente")]
    public Vector3 playerLastMovement;
    [Tooltip("Est ce que le player subis un wati boost ?")]
    public bool isBoosted = false;
    public float trueForwardSpeed;
    #region debug text
    [Header("Debug ou temporaire")]
    public long[] vibrationsDucul;
    #endregion

    Vector3 basePhoneAngle;
    Vector3 truePhoneDelta;
    Vector3 targetRotation;
    Vector3 forcesToAdd;
    Vector3 myEulerAngles;
    Vector3 startingPos;
    float addedSpeed;
    float xAccelerationDelta;
    float lastXRotation;
    float lastYPosition;
    public float actualForwardSpeed;
    bool invertHorizontal;
    bool invertVertical;
    float yDelta;
    float timeBeforeResetRotation;
    float zAccelerationDelta;
    float timeSpentInPique;
    float actualEssence;
    float timeSpendInWarudo;
    float maxWarudoTime;
    Rigidbody playerBody;

    #endregion

    int avgFrameRate;
    public bool tuto;

    private void CheckVars()
    {
        VarsChecker.CheckIfVarisAssigned(cam, gameObject);
        VarsChecker.CheckIfVarisAssigned(trigger, gameObject);
        VarsChecker.CheckIfVarisAssigned(controller, gameObject);
        VarsChecker.CheckIfVarisAssigned(follower, gameObject);
        VarsChecker.CheckIfVarisAssigned(topPlane, gameObject);
    }

    private void CheckPrefs()
    {
        if(PlayerPrefs.HasKey("InvertVertical"))
        {
            if(PlayerPrefs.GetInt("InvertVertical") == 1)
            {
                InvertVertical(true);
                verticalToggle.isOn = true;
            }

            else if (PlayerPrefs.GetInt("InvertVertical") == 0)
            {
                InvertVertical(false);
                verticalToggle.isOn = false;
            }
        }

        if (PlayerPrefs.HasKey("InvertHorizontal"))
        {
            if (PlayerPrefs.GetInt("InvertHorizontal") == 1)
            {
                InvertHorizontal(true);
                horizontalToggle.isOn = true;
            }

            else if (PlayerPrefs.GetInt("InvertHorizontal") == 0)
            {
                InvertHorizontal(false);
                horizontalToggle.isOn = false;
            }
        }

        if(PlayerPrefs.HasKey("RotationSpeed"))
        {
            SetRotationSpeed(PlayerPrefs.GetFloat("RotationSpeed"));
            rotationSlider.value = PlayerPrefs.GetFloat("RotationSpeed");
        }
    }

    public IEnumerator CheckSmartphoneAngle()
    {
        yield return new WaitForSeconds(.1f);
        basePhoneAngle = Input.acceleration;
    }

    public void ResetPhoneAngles()
    {
        basePhoneAngle = Input.acceleration;
    }

    public void SetRotationSpeed(float newSpeed)
    {
        rotationSpeed = newSpeed;
        PlayerPrefs.SetFloat("RotationSpeed", newSpeed);
    }

    public void GoToTopOfMap(Vector3 bottomplanepos)
    {
        Vector3 positionOffsetWithPlane = transform.position - bottomplanepos;
        Transform oldParent = follower.transform.parent;
        follower.transform.SetParent(transform);
        transform.position = new Vector3(transform.position.x, topPlane.transform.position.y, transform.position.z) + positionOffsetWithPlane;
        StartCoroutine(LeoMange(oldParent));
        lastYPosition = transform.position.y + positionOffsetWithPlane.y;
        addedSpeed = 0f;
        timeSpentInPique = 0f;
        energieAccumuleePique = 0f;
    }

    public IEnumerator LeoMange(Transform oldTransform)
    {
        yield return new WaitForEndOfFrame();
        follower.transform.SetParent(oldTransform);
    }

    private void Start()
    {
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
        Input.gyro.enabled = true;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        startingPos = transform.position;
        playerBody = GetComponent<Rigidbody>();
        StartCoroutine(CheckSmartphoneAngle());
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Gravity", new Vector3(0, gravity, 0));
        actualEssence = essenceDeSecours;
        maxWarudoTime = theWarudoSpeedCurve.keys[theWarudoTimeCurve.length - 1].time;
        CheckPrefs();
        CheckVars();
    }

    public void ChangePlayerGravity(float newGravityMultiplier)
    {
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Gravity", new Vector3(0, newGravityMultiplier, 0));
    }

    public void ResetPlayerGravity()
    {
        ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Gravity", new Vector3(0, gravity, 0));
    }

    public void ResetPlayer()
    {
        transform.rotation = Quaternion.identity;
        targetRotation = Vector3.zero;
        transform.position = startingPos;
        StartCoroutine(CheckSmartphoneAngle());
    }

    public void StartPlayer()
    {
        if(!tuto)
        {
            transform.rotation = Quaternion.identity;
        }
        targetRotation = Vector3.zero;
        StartCoroutine(CheckSmartphoneAngle());
        startingPos = transform.position;
    }

    private void FixedUpdate()
    {
        yDelta = lastYPosition - transform.position.y;
        Move();

        if(Time.frameCount % 4 == 0)
        {
            forcesToAdd = ForcesDictionnaryScript.forcesDictionnaryScript.ReturnAllForces();
#if !UNITY_EDITOR
            System.GC.Collect();
#endif
            if(playerBody.velocity != Vector3.zero)
            {
                playerBody.velocity = Vector3.zero;
            }
        }

#if !UNITY_EDITOR
        UpdatePlayer();
#endif


#if UNITY_EDITOR
        EditorControls();
#endif
        
        lastYPosition = transform.position.y;

    }



#region Gestion Deplacement

#region old
    //public IEnumerator TemporaryReturnOfDrop(float DropTime)
    //{
    //    yield return new WaitForSeconds(DropTime);
    //    mustMoveForward = true;
    //    ForcesDictionnaryScript.forcesDictionnaryScript.RemoveForce("Drop");
    //    actualPlayerState = PlayerState.pique;
    //    controller.StartPique();
    //}

    //public IEnumerator TemporaryEndAscend(float AscendTime)
    //{
    //    yield return new WaitForSeconds(AscendTime);
    //    mustMoveForward = true;
    //    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
    //    actualPlayerState = PlayerState.flying;
    //}

    //public void Drop()
    //{
    //    actualPlayerState = PlayerState.drop;
    //    mustMoveForward = false;
    //    ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("Drop", transform.forward * forwardSpeed / 100);
    //    transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, maxXRotation, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);
    //    timeSpentInPique += Time.deltaTime;
    //    // AJOUTER UNE LIGNE POUR RAMENER LA CAMERA EN POSITION AU DESSUS DU JOUEUR
    //    StartCoroutine(TemporaryReturnOfDrop(1.5f));
    //}
#endregion


    public void UpdatePlayer()
    {
        Vector3 phoneRotations = GetPhoneRotations();

        if (actualPlayerState == PlayerState.flying)
        {
            if (phoneRotations.x < -5f)
            {
                if (energieAccumuleePique > 0f)
                {
                    addedSpeed = Mathf.Exp(Mathf.Max(0, yDelta / 5)) * Mathf.Max(transform.eulerAngles.x / 100, 0) / 20;

                    energieAccumuleePique -= addedSpeed;
                    

                    if(energieAccumuleePique <= 0f)
                    {
                        energieAccumuleePique = 0f;
                        addedSpeed = 0f;
                    }
                }

                else if (actualEssence > 0f)
                {
                    actualEssence += yDelta;
                    Mathf.Clamp(actualEssence, -.2f, essenceDeSecours);
                }

                else
                {
                    //Drop();
                    return;
                }


            }

            else
            {
                if (actualEssence < essenceDeSecours)
                {
                    Mathf.Clamp(actualEssence, -.2f, essenceDeSecours);
                }
            }

#pragma warning disable CS0618 // Type or member is obsolete
            transform.RotateAround(Vector3.up, phoneRotations.y * Time.deltaTime * rotationSpeed);
#pragma warning restore CS0618 // Type or member is obsolete

            controller.SetOffset(phoneRotations.y);

            transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, phoneRotations.x, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);

            if (phoneRotations.x > 35)
            {
                actualPlayerState = PlayerState.pique;
                controller.StartPique();
            }
            

        }

        else if (actualPlayerState == PlayerState.pique)
        {
            if (phoneRotations.x < 35f)
            {

                if (timeSpentInPique > 1f)
                {
                    actualPlayerState = PlayerState.flying;
                    controller.StopPique();
                }

                else
                {
                    energieAccumuleePique += yDelta * piqueMultiplicator;
                }
            }

            else
            {
                energieAccumuleePique += yDelta * piqueMultiplicator;
            }

#pragma warning disable CS0618 // Type or member is obsolete
            transform.RotateAround(Vector3.up, phoneRotations.y * Time.deltaTime * rotationSpeed);
#pragma warning restore CS0618 // Type or member is obsolete
            

            transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, maxXRotation, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);
            timeSpentInPique += Time.deltaTime;

        }

        else if (actualPlayerState == PlayerState.interacting)
        {

#pragma warning disable CS0618 // Type or member is obsolete
            transform.RotateAround(Vector3.up, phoneRotations.y * Time.unscaledDeltaTime * rotationSpeed);
#pragma warning restore CS0618 // Type or member is obsolete
        }

    }

    public void Move()
    {
        if (mustMoveForward)
        {
            actualForwardSpeed = forwardSpeed - transform.forward.y * rotationInfluence + addedSpeed;
            playerLastMovement = actualForwardSpeed * transform.forward + forcesToAdd;
            playerBody.MovePosition(transform.position + playerLastMovement);
        }

        else
        {
            playerLastMovement = forcesToAdd;
            playerBody.MovePosition(transform.position + playerLastMovement);
        }
        trueForwardSpeed = transform.InverseTransformDirection(playerLastMovement).z;
        follower.SetOffset(trueForwardSpeed * 3);
    }


    public Vector3 GetPhoneRotations()
    {
        truePhoneDelta = Input.acceleration - basePhoneAngle;
        xAccelerationDelta = -truePhoneDelta.x;
        zAccelerationDelta = (truePhoneDelta.y - truePhoneDelta.z) / 2;

        xAccelerationDelta *= rotationCurve.Evaluate(Mathf.Abs(xAccelerationDelta));
        zAccelerationDelta *= rotationCurve.Evaluate(Mathf.Abs(zAccelerationDelta));


        //if (timeBeforeResetRotation > timeForBraquage)
        //{
        //    if (Mathf.Abs(lastXRotation - zAccelerationDelta) > .35f)
        //    {
        //        if (actualPlayerState == PlayerState.flying)
        //        {
        //            Drop();
        //        }

        //        else if (actualPlayerState == PlayerState.pique && zAccelerationDelta < .1f)
        //        {
        //            ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("FinPique", new Vector3(0f, energieAccumuleePique / timeSpentInPique, 0f), timeSpentInPique, false);
        //            actualPlayerState = PlayerState.forceAscend;
        //            StartCoroutine(TemporaryEndAscend(timeSpentInPique));
        //        }
        //    }

        //    else
        //    {
        //        pas de braquage
        //    }

        //    timeBeforeResetRotation = 0f;
        //    lastXRotation = zAccelerationDelta;
        //}

        //else
        //{
        //    timeBeforeResetRotation += Time.deltaTime;
        //}

        float newXRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, targetRotation.x);
        float newYRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, targetRotation.z);

        newXRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, zAccelerationDelta);

        newYRotation = Mathf.InverseLerp(-maxInputTaken, maxInputTaken, xAccelerationDelta);


        Vector3 calculatedVector = new Vector3(Mathf.Lerp(-maxXRotation, maxXRotation, newXRotation), Mathf.Lerp(-maxYRotation, maxYRotation, newYRotation), 0);

        if (!invertHorizontal)
        {
            calculatedVector.y *= -1;
        }

        if (invertVertical)
        {
            calculatedVector.x *= -1;
        }
        return calculatedVector;
    }

#endregion

    private void Update()
    {
        if(!UIManager.uimanager.isGamePaused && !EventSystem.current.IsPointerOverGameObject())
        {
#if !UNITY_EDITOR
                if (Input.touchCount > 0 && !trigger.activated && actualPlayerState == PlayerState.flying)
                {
                    long mabite = 10;
                    Vibration.Vibrate(mabite);
                    mustMoveForward = false;
                    trigger.Activate();
                    controller.SetInteractingBool(true);
                    actualPlayerState = PlayerState.interacting;
                    ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("ZAWARUDOFORCE", transform.forward * .02f);
            
                }

                else if (Input.touchCount > 0 && trigger.activated && timeSpendInWarudo < maxWarudoTime)
                {
                    timeSpendInWarudo += Time.unscaledDeltaTime;
                    TimeManager.timeManager.SlowTime(theWarudoTimeCurve.Evaluate(timeSpendInWarudo));
                }

                else if (Input.touchCount == 0 && trigger.activated)
                {
                    trigger.DeActivate();
                    ForcesDictionnaryScript.forcesDictionnaryScript.RemoveForce("ZAWARUDOFORCE");
                    mustMoveForward = true;
                    controller.SetInteractingBool(false);
                    actualPlayerState = PlayerState.flying;

                }

                else if (Input.touchCount == 0 && !trigger.activated && timeSpendInWarudo > 0f)
                {
                    timeSpendInWarudo -= Time.unscaledDeltaTime;
                    TimeManager.timeManager.SlowTime(theWarudoTimeCurve.Evaluate(timeSpendInWarudo));
                    ForcesDictionnaryScript.forcesDictionnaryScript.AddForce("TheWarudoSpeed", transform.forward * theWarudoSpeedCurve.Evaluate(timeSpendInWarudo));
                }

                else if (Input.touchCount == 0 && trigger.activated && actualPlayerState == PlayerState.interacting)
                {
                    ForcesDictionnaryScript.forcesDictionnaryScript.RemoveForce("ZAWARUDOFORCE");
                    mustMoveForward = true;
                    actualPlayerState = PlayerState.flying;

                }

#endif
        }
        myEulerAngles = returnGoodEulers(transform.eulerAngles);


        float current = 0;
        current = (int)(1f / Time.unscaledDeltaTime);
        avgFrameRate = (int)current;

#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && !trigger.activated && actualPlayerState == PlayerState.flying)
        {
            trigger.Activate();
            mustMoveForward = false;
            controller.SetInteractingBool(true);
            actualPlayerState = PlayerState.interacting;
        }

        else if (Input.GetMouseButtonUp(0) && trigger.activated)
        {
            isBoosted = true;
            trigger.DeActivate();
            mustMoveForward = true;
            controller.SetInteractingBool(false);
            actualPlayerState = PlayerState.flying;
        }

#endif
    }

    public Vector3 returnGoodEulers(Vector3 vectorToReturn)
    {
        if (vectorToReturn.x > 180)
        {
            vectorToReturn.x -= 360;
        }
        if (vectorToReturn.y > 180)
        {
            vectorToReturn.y -= 360;
        }
        if (vectorToReturn.z > 180)
        {
            vectorToReturn.z -= 360;
        }

        return vectorToReturn;

    }

    public void EnterCinematique()
    {
        actualPlayerState = PlayerState.cinematique;
        mustMoveForward = false;
    }

    public void LeaveCinematique()
    {
        actualPlayerState = PlayerState.flying;
        mustMoveForward = true;
    }

#region Debug

    public void EditorControls()
    {
        if(actualPlayerState != PlayerState.cinematique)
        {
            Vector3 mouseRotation = cam.ScreenToViewportPoint(Input.mousePosition);
            mouseRotation -= new Vector3(.5f, .5f);
            mouseRotation *= mouseScale;
            targetRotation = new Vector3(-mouseRotation.y, mouseRotation.x);


    #pragma warning disable CS0618 // Type or member is obsolete
            transform.RotateAround(Vector3.up, targetRotation.y * Time.deltaTime * Time.deltaTime);
    #pragma warning restore CS0618 // Type or member is obsolete
        

            transform.eulerAngles = new Vector3(Mathf.Lerp(myEulerAngles.x, targetRotation.x, rotationSpeed), transform.eulerAngles.y, transform.eulerAngles.z);


            if (targetRotation.x < 0f)
            {
                if (energieAccumuleePique > 0f)
                {
                    addedSpeed = Mathf.Exp(Mathf.Abs(yDelta)) * Mathf.Abs(targetRotation.x / 100);

                    energieAccumuleePique -= addedSpeed;

                    if (energieAccumuleePique <= 0f)
                    {
                        energieAccumuleePique = 0f;
                        addedSpeed = 0f;
                    }
                }
            }

            else
            {
                energieAccumuleePique += yDelta;
            }
        }
    }

    public void InvertHorizontal(bool doyou)
    {
        invertHorizontal = doyou;
        if (doyou == false)
        {
            PlayerPrefs.SetInt("InvertHorizontal", 0);
        }

        else
        {
            PlayerPrefs.SetInt("InvertHorizontal", 1);
        }
    }

    public void InvertVertical(bool doyou)
    {
        invertVertical = doyou;
        if (doyou == false)
        {
            PlayerPrefs.SetInt("InvertVertical", 0);
        }

        else
        {
            PlayerPrefs.SetInt("InvertVertical", 1);
        }
    }

#endregion

    public enum PlayerState
    {
        grounded,
        flying,
        interacting,
        drop,
        pique,
        forceAscend,
        cinematique
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPortals : MonoBehaviour
{
    public int level;
    public GameObject portalInterior;
    bool isActivated;
    

    void Start()
    {
        if (PlayerPrefs.HasKey("Level " + (level - 1).ToString()) && level > 1)
        {
            ActivatePortal();
        }

        else if(level == 1)
        {
            //ActivatePortal();
        }

#if UNITY_EDITOR
        else if (level <= 0 || level > 3)
        {
            Debug.LogWarning("The level the portal " + gameObject.name + " is linked to doesn't exist, check the inspector");
            Debug.Break();
        }
#endif

    }

    public void ActivatePortal()
    {
        isActivated = true;
        portalInterior.SetActive(true);
        //mettre le code pour l'intérieur des portails
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() != null && isActivated)
        {
            isActivated = false;
            MusicManager.musicManager.Win();
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 1);
            other.GetComponent<PlayerController>().cam.GetComponent<SecondCamera>().TransitionToNextLevel();
            MenuManager.menuManager.levelToLoad = level.ToString();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActivator : MonoBehaviour
{
    public bool activated;
    public float activationRadius;
    public float activationSpeed = 5f;
    public GameObject lichouzerie;
    public MeshRenderer blurPlane;
    Vector3 activationVector;

    private void Update()
    {
        if (activated && transform.localScale.x < activationRadius)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, activationVector, activationSpeed * Time.deltaTime);
        }

        if(!activated && transform.localScale.x > .01f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, activationSpeed * Time.unscaledDeltaTime);
        }
    }

    private void Start()
    {
        activationVector = new Vector3(activationRadius, activationRadius, activationRadius);
    }

    public void Activate()
    {
        activated = true;
        GetComponent<FMODUnity.StudioEventEmitter>().Play();
        lichouzerie.SetActive(true);
    }

    public void DeActivate()
    {
        activated = false;
        GetComponent<FMODUnity.StudioEventEmitter>().Stop();
        lichouzerie.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IActivatable>() != null)
        {
            Debug.Log("Activation !");
            other.GetComponent<IActivatable>().ActivateItem();
        }
    }
}

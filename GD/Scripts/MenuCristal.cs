﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCristal : MonoBehaviour, IActivatable
{
    public Animator mainCamAnim;
    public MenuPortals firstPortal;


    private bool hasBeenActivated;
    Animator anim;
    public ParticleSystem[] FXs;
    public ParticleSystem aura;
    public GameObject myLine;
    public PlayerController player;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void ActivateItem()
    {
        if(!hasBeenActivated)
        {
            mainCamAnim.SetBool("isInTuto", false);
            firstPortal.ActivatePortal();
            TimeManager.timeManager.SlowTime(1f);
            hasBeenActivated = true;
            anim.SetTrigger("Activation Trigger");
            player.forwardSpeed = .10f;
            GetComponent<FMODUnity.StudioEventEmitter>().SetParameter("Crystal 1", 1f);
            aura.Stop();
            CristauxActivation();
            if (myLine != null)
                myLine.SetActive(false);

        }
    }

    private void CristauxActivation()
    {
        // anim.Play();
        if (FXs.Length == 0)
        {
            return;
        }
        else
        {
            foreach (ParticleSystem fx in FXs)
            {
                fx.Play();
            }
        }
    }

}

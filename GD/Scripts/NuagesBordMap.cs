﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuagesBordMap : MonoBehaviour
{
    public Transform mapCenter;
    public float minSpeed;
    public float maxSpeed;

    float speed;

    private void Start()
    {
        speed = Random.Range(minSpeed, maxSpeed);
    }
    private void FixedUpdate()
    {
        transform.RotateAround(mapCenter.position, Vector3.up, speed * Time.deltaTime);
    }
}
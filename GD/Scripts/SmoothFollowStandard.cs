﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollowStandard : MonoBehaviour
{

    // The target we are following
    public Transform player;
    public Transform target;
    public Transform xMax;
    public Transform xmin;
    public Transform yMax;
    public Transform yMin;
    public Transform idlePos, PiqueTransform, MonteeTransform;
    public PlayerController PC;
    private bool SpecialState;
    // The distance in the x-z plane to the target
    public float distance = 10.0f;
    // the height we want the camera to be above the target
    public float height = 5.0f;
    public float approximatelyZero;
    // How much we 
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;
    public float camSpeed = 1f;

    

    public void UpdateCamera(float x, float y)
    {
        if (PC.actualPlayerState == PlayerController.PlayerState.pique || PC.actualPlayerState == PlayerController.PlayerState.drop || PC.actualPlayerState == PlayerController.PlayerState.forceAscend)
        {
            SpecialState = true;
        }


        // Early out if we don't have a target
        if (!target) return;
        Debug.Log(x + "" + y);
        // Calculate the current rotation angles
        float wantedRotationAngle = target.eulerAngles.y;
        float wantedHeight = target.position.y + height;

        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;

        // Damp the rotation around the y-axis
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

        // Damp the height
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        // Convert the angle into a rotation
        var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

        // Set the position of the camera on the x-z plane to:
    

        if(Mathf.Abs(x) < approximatelyZero)
        {
            target.position = Vector3.Lerp(target.position, idlePos.position, x * Time.deltaTime * 2 * camSpeed);
        }

        else
        {
            if (x > 0)
            {
                target.position = Vector3.Lerp(target.position, xMax.position, x * Time.deltaTime * camSpeed);
            }

            else
            {
                target.position = Vector3.Lerp(target.position, xmin.position, -x * Time.deltaTime * camSpeed);
            }
        }


        if (Mathf.Abs(y) < approximatelyZero)
        {
            target.position = Vector3.Lerp(target.position, idlePos.position, y * Time.deltaTime * 2 * camSpeed);
        }

        else
        {
            if (y > 0 && !SpecialState)
            {



                target.position = Vector3.Lerp(target.position, yMax.position, y * Time.deltaTime * camSpeed);
            }
           
            

            if(y< 0 && !SpecialState )
            {


                target.position = Vector3.Lerp(target.position, yMin.position, -y * Time.deltaTime * camSpeed);
            }

            if(y >0 && SpecialState)
            {
                if(PC.actualPlayerState == PlayerController.PlayerState.pique)
                {
                    target.position = new Vector3(target.position.x /3 , Mathf.Lerp(target.position.y, yMax.transform.position.y, y * Time.deltaTime * camSpeed) ,idlePos.position.z);
                   // target.position = Vector3.Lerp(target.position, PiqueTransform.position, -y * Time.deltaTime * 0.5f * camSpeed);
                }

              

            }
            if (y < 0 && SpecialState)
            {
                if (PC.actualPlayerState == PlayerController.PlayerState.pique)
                {
                    target.position = new Vector3(target.position.x /3, Mathf.Lerp(target.position.y, yMax.transform.position.y, -y * Time.deltaTime * camSpeed), idlePos.position.z);
                    //target.position = Vector3.Lerp(target.position, MonteeTransform.position, y * Time.deltaTime * 0.5f* camSpeed);
                }
            }
           

        }

        target.position = new Vector3 (target.position.x, target.position.y, idlePos.position.z);

        // distance meters behind the target
        transform.position = target.position;
        transform.position -= currentRotation * Vector3.forward * distance;


        // Set the height of the camera
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

        // Always look at the target
        transform.LookAt(player);
    }
}


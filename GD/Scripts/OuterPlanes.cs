﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuterPlanes : MonoBehaviour
{
    public bool isTeleportPlane;
    public float distanceToPlayer;
    public Transform player;
    public AnimationCurve dissolveAC;
    Material planeMat;
    float minValue;

    private void Start()
    {
        if(!isTeleportPlane)
        {
            planeMat = GetComponent<MeshRenderer>().material;
            GetComponent<MeshRenderer>().material = planeMat;
            minValue = dissolveAC.keys[dissolveAC.length - 1].value;
            Debug.Log(minValue);
        }

    }

    private void FixedUpdate()
    {
        distanceToPlayer = player.position.y - transform.position.y;

        transform.position = new Vector3(player.position.x, transform.position.y, player.position.z);
        
        if(!isTeleportPlane && distanceToPlayer < 30f)
        {
            
                planeMat.SetFloat("Vector1_4E3BADC6", dissolveAC.Evaluate(distanceToPlayer));

            
        }

        else if(!isTeleportPlane && planeMat.GetFloat("Vector1_4E3BADC6") != minValue)
        {
            planeMat.SetFloat("Vector1_4E3BADC6", minValue);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() != null && isTeleportPlane)
        {
            other.GetComponent<PlayerController>().GoToTopOfMap(transform.position);
        }
    }
}
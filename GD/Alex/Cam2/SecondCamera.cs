﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class SecondCamera : MonoBehaviour
{
    public Transform target;
    public float valueLimit;
    Vector3 lookOffset;
    Vector3 localOffset;
    private PlayerController playerC;
    private Animator animator;
    public GameObject player;
    public GameObject falsePlayer;

    bool ended;
    public bool tuto;

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        playerC = player.GetComponent<PlayerController>();
        ended = true;
        animator.SetBool("isInTuto", tuto);
    }
    
    private void LateUpdate()
    {
        if(!ended)
        {
            transform.LookAt(target.position + lookOffset);
            PlaySpeedAnim();
        }

        if(tuto)
        {
            transform.LookAt(target.position + lookOffset);

        }
    }
    public void SetIsBoostedFalse()
    {
        playerC.isBoosted = false;
    }
    public void PlaySpeedAnim()
    {

        /*   if((playerC.playerLastMovement.y <= -0.36f) || (playerC.playerLastMovement.y >= 0.36f))
           {

               animator.speed = 1;
               animator.SetBool("speedBool", true);
           }

           if ((Mathf.Abs(playerC.playerLastMovement.y)) >= 0.31f && (Mathf.Abs(playerC.playerLastMovement.y)) < 0.36f)
           {

               animator.speed = 0.6f;
               animator.SetBool("speedBool", true);
           }


           if ((Mathf.Abs(playerC.playerLastMovement.y)) >= 0.26f && (Mathf.Abs(playerC.playerLastMovement.y)) < 0.31f)
           {

               animator.speed = 0.35f;
               animator.SetBool("speedBool", true);
           }

           if ((Mathf.Abs(playerC.playerLastMovement.y)) >= 0.18f && (Mathf.Abs(playerC.playerLastMovement.y)) < 0.26f)
           {

               animator.speed = 0.2f;
               animator.SetBool("speedBool", true);
           }
           if ((Mathf.Abs(playerC.actualForwardSpeed)) < 0.22f)
           {

               animator.SetBool("speedBool", false);
           }
           */
        if ((Mathf.Abs(playerC.trueForwardSpeed)) < 0.35f)
        {

            animator.SetBool("speedBool", false);
        }
        if ((Mathf.Abs(playerC.trueForwardSpeed)) >= 0.3f)
        {
            animator.SetBool("speedBool", true);
            animator.speed = Mathf.Min(playerC.trueForwardSpeed, 1f);
        }

     /*   if (playerC.isBoosted)
        {
            animator.SetBool("speedBool", false);
            animator.SetBool("boostBool", true);
            animator.speed = 1.5f;
            SetIsBoostedFalse();
        }
        */
    }

    public void TransitionToNextLevel()
    {
        ended = true;
        falsePlayer.SetActive(true);
        falsePlayer.transform.position = player.transform.position;
        falsePlayer.transform.rotation = player.transform.rotation;
        player.SetActive(false);
        falsePlayer.GetComponentInChildren<Animator>().SetBool("CineDive", true);
        animator.Play("Level1_Transition_SecondCamera");
    }

    public void LoadNextScene()
    {
        if(!tuto)
        {
            MusicManager.musicManager.ReleaseMusic();
            int nextlevel;
            int.TryParse(MusicManager.musicManager.World, out nextlevel);
            nextlevel++;
            SceneManager.LoadScene("Level " + (nextlevel).ToString());
        }

        else
        {
            MenuManager.menuManager.LoadGoodScene();
        }

    }

    public void StartGame()
    {
        player.SetActive(true);
        transform.localPosition = Vector3.zero;
        transform.LookAt(target.position + lookOffset);
        ended = false;
        MusicManager.musicManager.GameStarted();
    }


    public void ChangeLookOffset(float xOffset, float yOffset, float zOffset)
    {
        lookOffset = new Vector3(xOffset, yOffset, zOffset);
    }


}

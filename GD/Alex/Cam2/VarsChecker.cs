﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class VarsChecker
{

    public static void CheckIfVarisAssigned<T>(T varToCheck, GameObject objectToCheck)
    {
        if (varToCheck == null)
        {
            #if UNITY_EDITOR
                if(EditorApplication.isPlaying)
                {
                    Debug.Break();
                }
                Selection.SetActiveObjectWithContext(objectToCheck, null);
            #endif
            Debug.LogError("A variable is not assigned on gameobject " + objectToCheck.name);
        }
    }
}
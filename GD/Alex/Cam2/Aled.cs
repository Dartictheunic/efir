﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aled : MonoBehaviour
{
    float amplitude;
    float speed;

    private void Start()
    {
        amplitude = Random.Range(.01f, .02f);
        speed = Random.Range(.01f, .02f);
    }

    private void FixedUpdate()
    {
        transform.position += new Vector3(0, Mathf.Sin(Time.time * amplitude) * speed, 0);
    }
}
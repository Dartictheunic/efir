﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Homebrew;

public class PlayerFollower : MonoBehaviour
{
    public Transform behindPlayer;
    public Transform player;
    [Tooltip("Vitesse à laquelle la caméra s'avance vers le joueur en cas de hit")]
    public float blockedSpeed;
    public float targetOffset;
    Vector3 wantedPos;
    Vector3 direction;
    public AnimationCurve zCurve;
    public float noise;
    public float noise2;

    public void Update()
    {
        wantedPos = behindPlayer.position;// + (targetOffset * player.forward);
        //noise = Mathf.PerlinNoise(Time.time/10, 0);
        //noise -= .5f;
        //wantedPos += transform.right * noise * Mathf.Exp(targetOffset) * 10;
        //noise2 = Mathf.PerlinNoise(0, Time.time / 10);
        //noise2 -= .5f;
        //wantedPos += transform.up * noise2 * Mathf.Exp(targetOffset) * 10;
        
        if (Input.GetKey(KeyCode.T))
        {
            targetOffset += Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.Y))
        {
            targetOffset -= Time.deltaTime;
        }

        direction = wantedPos - transform.position;
        //zCurve.AddKey(Time.time, Mathf.Abs(wantedPos.z));
    }

    public void LateUpdate()
    {
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, direction.magnitude, ~0, QueryTriggerInteraction.Ignore))
        {
            transform.position = Vector3.Lerp(transform.position, wantedPos, Time.deltaTime * 3);
        }
        else
        {
            Vector3 directionSafe = behindPlayer.position - player.position;
            Ray raySafe = new Ray(player.position, directionSafe);
            Debug.DrawRay(player.position, directionSafe, Color.red, 5f, false);
            RaycastHit hitSafe;

            if (!Physics.Raycast(raySafe, out hitSafe, directionSafe.magnitude, ~0, QueryTriggerInteraction.Ignore))
            {
                transform.position = Vector3.Lerp(transform.position, wantedPos, Time.deltaTime * 3);
            }

            else if(!hit.collider.isTrigger)
            {
                transform.position = Vector3.Lerp(transform.position, hitSafe.point, Time.deltaTime * 3);
            }
        }
    }

    public void SetOffset(float offsetToSet)
    {
        targetOffset = offsetToSet;
    }
}
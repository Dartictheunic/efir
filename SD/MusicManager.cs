﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string music = "event:/BGM";

    FMOD.Studio.EventInstance musicEvent;

    public string World;

    public static MusicManager musicManager;
    public PlayerController player;

    float normalVolume;
    bool fadeOutMusic;

    // Start is called before the first frame update
    void Start()
    {
        var musicToLoad = music + "_" + World;
        musicEvent = FMODUnity.RuntimeManager.CreateInstance(musicToLoad);
        musicEvent.start();
        musicManager = this;
        normalVolume = 1f;
    }

    //Player starts the game
    public void GameStarted()
    {
        musicEvent.setParameterValue("Game Started", 1f);
        player.StartPlayer();
        player.mustMoveForward = true;
    }


    //Player récupère 1er crystal
    public void Crystal(int number)
    {
        if (number == 1)
        {
            musicEvent.setParameterValue("Crystal " + number.ToString(), 1f);
        }

        else
        {
            musicEvent.setParameterValue("Crystal " + (number - 1).ToString(), 0f);
            musicEvent.setParameterValue("Crystal " + number.ToString(), 1f);
        }
    }

    public void InCourantDAir(float value)
    {
        musicEvent.setParameterValue("isInCourantdAir", value);
    }

    public void Win()
    {
        fadeOutMusic = true;
    }

    public void ToGameEnd()
    {
        musicEvent.setParameterValue("Game End", 1f);
    }

    public void ReleaseMusic()
    {
        musicEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        musicEvent.release();
    }

    private void FixedUpdate()
    {
        if(fadeOutMusic)
        {
            musicEvent.setVolume(normalVolume);
            normalVolume -= Time.unscaledDeltaTime  /  10;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CinematiqueCamera : MonoBehaviour
{
    public bool isBeginning = false , canControlEfir = true;
    public Camera mainCam;
    public Camera cineCam;
    public GameObject mB_Plane, player;
    public Animator CH_EFIR;
    public float AnimSpeed;
    private Animator anim;
    public bool fin;
    // Start is called before the first frame update

        // Alors : Il va falloir activer la Caméra avant d'éxecuter la fonction switchCam !
        //Si la liche se fait pas assez vite efir partira via code non ?
    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        anim = gameObject.GetComponent<Animator>();
        string sceneName = currentScene.name;
        if(sceneName == "Menu")
        {
            CH_EFIR.SetBool("EfirAsleep", true) ;
        }
        if(fin == true){
            anim.SetBool("EndScene", true);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        CH_EFIR.speed = AnimSpeed;
        if(fin == false)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                gameObject.GetComponent<Animator>().SetBool("Begin", true);
            }

            if (canControlEfir == false)
            {
                player.GetComponent<PlayerController>().actualPlayerState = PlayerController.PlayerState.cinematique;
            }

            if (canControlEfir == true)
            {
                player.GetComponent<PlayerController>().actualPlayerState = PlayerController.PlayerState.flying;

            }
        }
    
    }

    public void EfirDive()
    {

        player.GetComponent<PlayerController>().controller.StartDiveCinematique();
    }

    public void EfirWake()
    {
        Debug.Log("Wake");
        CH_EFIR.SetBool("CineWakeUp", true);
        CH_EFIR.SetBool("EfirAsleep", false);
    }
    

    public void EfirLeap()
    {
        CH_EFIR.GetComponent<Animator>().SetBool("CineWakeUp", true);
    }

    public void SwitchCam()
    {
        canControlEfir = false;
        cineCam.transform.position = mainCam.transform.position;
        cineCam.transform.rotation = mainCam.transform.rotation;
        cineCam.fieldOfView = mainCam.fieldOfView;
        mB_Plane.transform.SetParent(gameObject.transform);
        player.transform.SetParent(gameObject.transform);
        mainCam.gameObject.SetActive(false);

    }

    public void TutoSwitchCam()
    {
        canControlEfir = true;
        mainCam.gameObject.SetActive(true);
        MenuManager.menuManager.skipButton.SetActive(false);
        player.SetActive(false);
        mainCam.GetComponent<SecondCamera>().StartGame();
        cineCam.gameObject.SetActive(false);
    }

    public void EndingSwitch()
    {
        mainCam.gameObject.SetActive(true);
        player.gameObject.SetActive(false);

        mB_Plane.SetActive(true);
        cineCam.gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionBlue_Plane_Script : MonoBehaviour
{
    public float flouFloat, smStep1Float, opacityFloat;
    Material mat;
    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        GetComponent<MeshRenderer>().material = mat;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        mat.SetFloat("_flou",flouFloat);
        mat.SetFloat("_smstep", smStep1Float);
        mat.SetFloat("_opacity", opacityFloat);
    }
}

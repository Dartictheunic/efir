﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LastCameraScript : MonoBehaviour
{
    public Animator EFIR_Anim, KAFFIR_Anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void WakePute()
    {
        EFIR_Anim.SetBool("WakeUp", true);
        KAFFIR_Anim.SetBool("WakeUp", true);
    }


    public void GoMenu()
    {
        SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

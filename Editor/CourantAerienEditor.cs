﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor  (typeof(CourantAerien))]
public class CourantAerienEditor : Editor
{
    private CourantAerien cA; 

    private void OnSceneGUI()
    {
        cA = target as CourantAerien;

        switch (cA.type)
        {
            case TypeDeCourant.ascendant:
                {
                    cA.typeModif = 1;
                    //Debug.Log (cA.typeModif);
                    cA.fx.transform.position = cA.entree.transform.position;
                    ParticleSystem.VelocityOverLifetimeModule psVel = cA.fx.velocityOverLifetime;
                    psVel.yMultiplier = cA.particlesSpeed*cA.typeModif;
                    ParticleSystem.MainModule psMain = cA.fx.main;
                    psMain.startColor = cA.cAscendant;
                    cA.meshColor = cA.meshColorAscend;
                }
                break;

            case TypeDeCourant.descendant:
                {
                    cA.typeModif = -1;
                    //Debug.Log(cA.typeModif);
                    cA.fx.transform.position = cA.sortie.transform.position;
                    ParticleSystem.VelocityOverLifetimeModule psVel = cA.fx.velocityOverLifetime;
                    psVel.yMultiplier = cA.particlesSpeed * cA.typeModif;
                    ParticleSystem.MainModule psMain = cA.fx.main;
                    psMain.startColor = cA.cDescandant;
                    cA.meshColor = cA.meshColorDescend;
                }
                break;


        }
        

        if (Application.isPlaying == false)
        {

        EditorUtility.SetDirty(cA);
        EditorUtility.SetDirty(cA.transform);
        EditorUtility.SetDirty(cA.fx);
        EditorSceneManager.MarkSceneDirty(cA.gameObject.scene);

        }

        else
        {
            return;
        }
        

       
    }
}
